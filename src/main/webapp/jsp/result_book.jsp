<%@page contentType="text/html;charset = UTF-8" language="java"%>
<%@page isELIgnored="false"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<title>Book Result</title>
</head>

<body>
	<h2>Submitted Book Information</h2>
	<table>
		<tr>
			<td>Title</td>
			<td>${book.title}</td>
		</tr>
		<tr>
			<td>Author</td>
			<td>${book.author.name} ${book.author.surname}</td>
		</tr>
		<tr>
			<td>Publication Date:</td>
			<td><fmt:formatDate value="${book.publicationDate}"
					pattern="dd-MM-yyyy" /></td>
	</table>
	<form:form method="get" action="/SpringMVC/">
		<td colspan="2"><input type="submit" value="Return to main page" /></td>
	</form:form>
</body>
</html>