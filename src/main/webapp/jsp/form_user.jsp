<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h2>Submit User Information</h2>
	<form:form method="post" action="/SpringMVC/User">
		<table>
			<tr>
				<td><form:label path="nick">Nick</form:label></td>
				<td><form:input path="nick" /></td>
				<td><form:errors path="nick" cssclass="error"></form:errors></td>
			</tr>
			<tr>
				<td><form:label path="email">email</form:label></td>
				<td><form:input path="email" /></td>
				<td><form:errors path="email" cssclass="error"></form:errors></td>
			</tr>
			<tr>
				<td><form:label path="password">password</form:label></td>
				<td><form:input path="password" /></td>
				<td><form:errors path="password" cssclass="error"></form:errors></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form:form>
</body>

</html>