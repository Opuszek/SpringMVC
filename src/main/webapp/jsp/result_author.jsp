<%@page contentType = "text/html;charset = UTF-8" language = "java" %>
<%@page isELIgnored = "false" %>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
   <head>
      <title>Author Result</title>
   </head>

   <body>
      <h2>Submitted Author Information</h2>
      <table>
         <tr>
            <td>Name</td>
            <td>${author.name}</td>
         </tr>
         <tr>
            <td>Surname</td>
            <td>${author.surname}</td>
         </tr>
         </table>
         <h3>Books:</h3>
         <ul>
         <c:forEach var="book" items="${author.bookSet}">
         <li>${book.title}</li>
         </c:forEach>
         </ul>

	<form:form method="get" action="/SpringMVC/">
		<td colspan="2"><input type="submit" value="Return to main page" /></td>
	</form:form>


</body>
   
</html>