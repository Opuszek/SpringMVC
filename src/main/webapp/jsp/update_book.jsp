<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Update Book</title>
</head>

<body>
	<h2>Submit Update Information</h2>
	<form:form method="POST" action="/SpringMVC/updatedBook">
		<table>
			<tr>
				<td><form:label path="title">Title ${book.id}</form:label></td>
				<td><form:input path="title" /></td>
			</tr>
			<tr>
				<form:hidden path="id" value="${book.id}" />
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form:form>
</body>

</html>