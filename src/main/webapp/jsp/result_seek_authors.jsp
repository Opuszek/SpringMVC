<%@page contentType="text/html;charset = UTF-8" language="java"%>
<%@page isELIgnored="false"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Authors Result</title>
</head>

<body>
	<h2>Founded Authors</h2>

	<table>
		<c:forEach var="author" items="${list}">
			<tr>
				<td>${author.name}</td>
				<td>${author.surname}</td>
				<form:form method="delete" action="/SpringMVC/Author">
					<form:hidden path="id" value="${author.id}" />
					<form:hidden path="name" value="${keyword}" />
					<!-- name works here as a contener for a search keyword -->
					<td colspan="2"><input type="submit" value="Delete" /></td>
				</form:form>
				<form:form method="get" action="/SpringMVC/Author">
					<input type=hidden name="id" value="${author.id}" />
					<td><input type="submit" value="Update" name="Submit" /></td>
				</form:form>
			</tr>
		</c:forEach>

	</table>

</body>

</html>