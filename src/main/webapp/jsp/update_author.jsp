<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Update Book</title>
</head>

<body>
	<h2>Submit Update Information</h2>
	<form:form method="POST" action="/SpringMVC/updatedAuthor">
		<table>
			<tr>
				<td><form:label path="name">Name</form:label></td>
				<td><form:input path="name" /></td>
			</tr>
			<tr>
				<td><form:label path="surname">Surname</form:label></td>
				<td><form:input path="surname" /></td>
			</tr>
			<tr>
				<form:hidden path="id" value="${id}" />
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form:form>
</body>

</html>