<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registration</title>
</head>
<body>
	<form:form method="POST" action="/SpringMVC/registration">
		<table>
			<tr>
				<td><form:label path="nick">nick</form:label></td>
				<td><form:input path="nick" /></td>
				<td><form:errors path="nick" cssclass="error"></form:errors></td>
				<td><form:errors path="" cssclass="error"></form:errors></td>
			</tr>
			<tr>
				<td><form:label path="password">Password</form:label></td>
				<td><form:password path="password" /></td>
				<td><form:errors path="password" cssclass="error"></form:errors></td>
			</tr>
			<tr>
				<td><form:label path="confirmPassword">Confirm password</form:label></td>
				<td><form:password path="confirmPassword" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form:form>
</html>