<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<title>Add Book</title>
</head>

<body>
	<h2>Submit Book Information</h2>
	<form:form method="${method}" action="/SpringMVC/Book">
		<input type=hidden name="authorList" value="${authorList}">
		<form:hidden path="id" value="${id}" />
		<fieldset>
			<table>
				<tr>
					<td><form:label path="title">Title </form:label> <form:input
							path="title" /> <form:errors path="title" cssclass="error" />
							<form:errors path="" cssclass="error" /></td>
				</tr>
				<tr>
					<td><form:label path="publicationDate">PublicationDate:</form:label>
						<fmt:formatDate value="${now}" pattern="dd-MM-yyyy"
							var="datePattern" /> <form:input type="text"
							path="publicationDate" class="date" value="${datePattern}" /> <form:errors
							path="publicationDate" cssclass="error" /></td>
				</tr>
				<tr>
					<td><form:select path="author.id">
							<c:forEach var="author" items="${authorList}">
								<form:option
									selected="${author.id eq author_id ? 'selected' : ''}"
									value="${author.id}" label="${author.name}" />
							</c:forEach>
						</form:select></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="Submit" /></td>
				</tr>
			</table>
		</fieldset>
	</form:form>
</body>

</html>