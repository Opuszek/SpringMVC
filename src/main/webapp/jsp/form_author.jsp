<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Add Author</title>
</head>

<body>
	<h2>Submit Author Information</h2>
	<form:form method="${method}" action="/SpringMVC/Author">
		<table>
			<tr>
				<td><form:label path="name">Name</form:label></td>
				<td><form:input path="name" /></td>
				<td><form:errors path="name" cssclass="error"></form:errors></td>
				<td><form:errors path="" cssclass="error"></form:errors></td>
			</tr>
			<tr>
				<td><form:label path="surname">Surname</form:label></td>
				<td><form:input path="surname" /></td>
				<td><form:errors path="surname" cssclass="error"></form:errors></td>
			</tr>
			<tr>
				<form:hidden path="id" value="${id}" />
				<!-- for update transaction-->
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form:form>
</body>

</html>