<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Search Author</title>
</head>

<body>
	<h2>Submit Search Author Information</h2>
	<form:form method="POST" action="/SpringMVC/searchAuthors">
		<tr>
			<td>Keyword</td>
			<td><input type="text" name="keyword"></td>
			<td colspan="2"><input type="submit" value="Submit" /></td>
		</tr>
	</form:form>
</body>

</html>