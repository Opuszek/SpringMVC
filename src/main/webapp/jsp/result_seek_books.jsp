<%@page contentType="text/html;charset = UTF-8" language="java"%>
<%@page isELIgnored="false"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Book Result</title>
</head>

<body>
	<h2>Founded Books</h2>

	<table>
		<tr>
			<td>Title</td>
			<td>Publication Date</td>
			<td>Author</td>
		</tr>
		<c:forEach var="book" items="${list}">
			<tr>
				<td>${book.title}</td>
				<td>${book.publicationDate}</td>
				<td>${book.author.name} ${book.author.surname}</td>
				<form:form method="delete" action="/SpringMVC/Book">
					<form:hidden path="id" value="${book.id}" />
					<form:hidden path="title" value="${keyword}" />
					<!-- title works here as a contener for a search keyword -->
					<td colspan="2"><input type="submit" value="Delete" /></td>
				</form:form>
				<form:form method="get" action="/SpringMVC/Book">
					<form:hidden path="id" value="${book.id}" />
					<input type="hidden" name="updated_author_id"
						value="${book.author.id}" />
					<td><input type="submit" value="Update" name="Submit" /></td>
				</form:form>
			</tr>
		</c:forEach>
	</table>


</body>

</html>