<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<body>

	<c:url value="/j_spring_security_logout" var="logoutUrl" />
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
	<script>
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
	</script>

	<c:if test="${pageContext.request.userPrincipal.name != null}">
	Welcome : ${pageContext.request.userPrincipal.name} 
	</c:if>

	<ol>
		<form:form action="/SpringMVC/Book" method="GET">
			<input type="submit" value="Book" />
		</form:form>
		<form:form action="/SpringMVC/Author" method="GET">
			<input type="submit" value="Author" />
		</form:form>
		<form:form action="/SpringMVC/searchAuthors" method="GET">
			<input type="submit" value="search Authors" />
		</form:form>
		<form:form action="/SpringMVC/searchBooks" method="GET">
			<input type="submit" value="search Books" />
		</form:form>
	</ol>

	<form:form action="/SpringMVC/registration" method="GET">
		<input type="submit" value="register">
	</form:form>
	
	<form:form action="/SpringMVC/login" method="GET">
		<input type="submit" value="login">
	</form:form>

	<c:if test="${pageContext.request.userPrincipal.name != null}">
		<input type="submit" value="log out" onclick="javascript:formSubmit()" />
	</c:if>



</body>
</html>
