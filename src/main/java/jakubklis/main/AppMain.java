package jakubklis.main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import jakubklis.configuration.HibernateConfiguration;
import jakubklis.dao.AuthorDao;
import jakubklis.dao.UserDao;
import jakubklis.model.Author;
import jakubklis.service.BookService;

@SuppressWarnings("unused")
public class AppMain {

	private static final Logger LOGGER = LogManager.getLogger(AppMain.class.getName());
	  
	public static void main(String args[]) {
		AbstractApplicationContext context = new AnnotationConfigApplicationContext(HibernateConfiguration.class);
		AuthorDao authorDao = (AuthorDao) context.getBean(AuthorDao.class);
		BookService bookService = (BookService) context.getBean(BookService.class);
		UserDao userDao = (UserDao) context.getBean(UserDao.class);
		Author author = new Author();
		author.setName("wef");
		author.setSurname("erger");
        authorDao.save(author);
        context.close();

	}
}
