package jakubklis.service;

import java.util.Date;
import java.util.List;

import jakubklis.model.Book;

public interface BookService {

	public int save(String title, int author_id, Date publicationDate);

	public void update(int id, String title, Integer author_id, Date publicationDate);

	public Book getById(int id);

	public void deleteById(int id); 
	
	public List<Book> list();
	
	public List<Book> seek(String string);
	
	public boolean exists (Book book);
}