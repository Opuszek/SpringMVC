package jakubklis.service;

import java.util.List;

import jakubklis.model.Author;
 
 
public interface AuthorService {
 
    public int save(String name, String surname);
 
    public void update ( int id, String title, String surname );
    
    public Author getById (int id);
    
    public boolean deleteById (int id);
    
    public List<Author> list();
    
    public List<Author> seek(String string);
    
    public boolean exists (Author author);
}