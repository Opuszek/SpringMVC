package jakubklis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jakubklis.dao.UserDao;
import jakubklis.model.Role;
import jakubklis.model.User;

@Service("UserService")
@Transactional
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	
	public User getUserByNick(String nick){
		return userDao.getUserByNick(nick);
	}
	
	public int save (User user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		Role role = new Role();
		role.setUser(user);
		role.setRole("ROLE_USER");
		user.setRole(role);
		return userDao.save(user);		
	}

}
