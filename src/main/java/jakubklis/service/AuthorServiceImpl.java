package jakubklis.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jakubklis.dao.AuthorDao;
import jakubklis.model.Author;


@Service("AuthorService")
@Transactional
public class AuthorServiceImpl implements AuthorService {
	
	@Autowired
	private AuthorDao authorDao;

	@Override
    public int save(String name, String surname) {
    	Author author = new Author();
    	author.setName(name);
    	author.setSurname(surname);
    	return authorDao.save(author);
    }
    
	@Override
    public void update (int id, String name, String surname) {	
    	Author author = authorDao.get(id);
    	if (name!=null) author.setName(name);
    	if (surname!=null) author.setSurname(surname);
    	authorDao.update(author);
    }
	
	@Override
	public boolean exists (Author author) {
	    return authorDao.find(author.getName(), author.getSurname())!=null;
	}
    
	@Override
    public Author getById (int id) {
    	return authorDao.get(id);
    } 
    
	@Override
    public boolean deleteById (int id) {
    	return authorDao.deleteById(id);
    }
    
	@Override
    public List<Author> list(){
    	return authorDao.list();
    }
	@Override
	public List<Author> seek(String string) {
		return authorDao.seek(string);
	}
}
