package jakubklis.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jakubklis.dao.BookDao;
import jakubklis.model.Author;
import jakubklis.model.Book;

@Service("BookService")
@Transactional
public class BookServiceImpl implements BookService {

	@Autowired
	private BookDao bookDao;
	
	@Override
	public int save(String title, int author_id, Date publicationDate) {
		Book book = new Book();
		Author author = new Author();
		author.setId(author_id);
		book.setTitle(title);
		book.setAuthor(author);
		book.setPublicationDate(publicationDate);
		return bookDao.save(book);
	}

	@Override
	public void update(int id, String title, Integer author_id, Date publicationDate) {
		Book book = bookDao.get(id);
		Logger logger = LogManager.getLogger();
		logger.log(Level.INFO, book);
		logger.log(Level.INFO, id);
		Author author = new Author();
		if (title!=null) book.setTitle(title);
		if (publicationDate!=null) book.setPublicationDate(publicationDate);
		if (author_id!=null) {
			author.setId(author_id);
			List<Book>list = new ArrayList<Book>();
			list.add(book);
			author.setBookSet(new HashSet<Book>(list));
			book.setAuthor(author);
		}
		bookDao.update(book);
	}
	
	@Override
	public boolean exists(Book book) {
	    return bookDao.find(book.getTitle(), book.getPublicationDate())!=null;
	}

	@Override
	public void deleteById(int id) {
		 bookDao.deleteById(id);
	}

	@Override
	public Book getById(int id) {
		return bookDao.get(id);
	}

	@Override
	public List<Book> list() {
		return bookDao.list();
	}
	@Override
	public List<Book> seek(String string){
		return bookDao.seek(string);
	}
}