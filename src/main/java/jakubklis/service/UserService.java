package jakubklis.service;

import jakubklis.model.User;

public interface UserService {
	
	public User getUserByNick (String nick);
	public int save (User user);
	
	
	
}
