package jakubklis.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import jakubklis.validator.UniqueEntityConstraint;

@Entity
@Table(name = "Books", uniqueConstraints = { @UniqueConstraint(columnNames = { "title", "publication_date" }) })
@UniqueEntityConstraint
public class Book {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NotEmpty
    @Length(max = 40)
    private String title;

    @Temporal(TemporalType.DATE)
    @NotNull
    @Past
    @Column(name = "publication_date")
    private Date publicationDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "author_id")
    private Author author;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public void setPublicationDate(Date date) {
        this.publicationDate = date;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    @Override
    public String toString() {
        return "id=" + id + ", title=" + title + ", publicationDate=" + publicationDate + ", author= "
                + ((author == null) ? "" : author.getName() + " " + author.getSurname());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Book))
            return false;
        Book book = (Book) obj;
        return Objects.equals(title, book.getTitle()) && Objects.equals(publicationDate, book.getPublicationDate());
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + Objects.hashCode(title);
        result = 31 * result + Objects.hashCode(publicationDate);
        return result;
    }
}
