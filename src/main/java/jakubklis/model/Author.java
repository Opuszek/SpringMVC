package jakubklis.model;


import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import jakubklis.validator.UniqueEntityConstraint;


@Entity
@Table(name = "Authors", uniqueConstraints = { @UniqueConstraint(columnNames = { "name", "surname" }) })
@UniqueEntityConstraint
public class Author {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	
	@NotEmpty
	@Length(max=40)
	private String name;
	
	@NotEmpty
	@Length(max=40)
	private String surname;
	
	
	@OneToMany(mappedBy = "author",fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval=true)
	@Fetch (FetchMode.SELECT) 
	private Set<Book>bookSet;

	
	
	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getSurname() {
		return surname;
	}



	public void setSurname(String surname) {
		this.surname = surname;
	}


	
	public Set<Book> getBookSet() {
		return bookSet;
	}



	public void setBookSet(Set<Book> bookSet) {
		this.bookSet = bookSet;
	}



	@Override
	public String toString(){
		// return "id="+id+", name="+ name + " surname: " + surname + ", books="+ bookList;
		return name + surname;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj==null) return false;
		if (!(obj instanceof Author)) return false;
		Author author = (Author) obj;
		return Objects.equals(name, author.getName()) && 
				Objects.equals(surname, author.getSurname());
	}
	
    public int hashCode() {
        int result = 17;
        result = 31 * result + Objects.hashCode(name);
        result = 31 * result + Objects.hashCode(surname);
        return result;
    }
    
}
