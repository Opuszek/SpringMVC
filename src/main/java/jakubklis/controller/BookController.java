package jakubklis.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import jakubklis.model.Book;
import jakubklis.service.AuthorService;
import jakubklis.service.BookService;

@Controller
public class BookController {

	@Autowired
	AuthorService authorService;

	@Autowired
	BookService bookService;

	@RequestMapping(value = "/Book", method = RequestMethod.GET)
	public ModelAndView getSaveBookForm( ModelMap model) {
		model.addAttribute("method", "post");
		model.addAttribute("authorList", authorService.list());
		return new ModelAndView("form_book", "command", new Book());
	}
	
	@RequestMapping(value = "/Book", method = RequestMethod.GET, params = "id")
	public ModelAndView getUpdateBookForm( @RequestParam(defaultValue = "0") Integer id,
            @RequestParam(defaultValue = "0") Integer updated_author_id, ModelMap model) {
        model.addAttribute("id", id);
        model.addAttribute("author_id", updated_author_id);
        model.addAttribute("method", "put");
        model.addAttribute("authorList", authorService.list());
        return new ModelAndView("form_book", "command", new Book());
    }

	@RequestMapping(value = "/Book/{id}", method = RequestMethod.GET)
	public String getBookData(@PathVariable(value = "id") int id, ModelMap model) {
		Book book = bookService.getById(id);
		model.addAttribute("book", book);
		return "result_book";
	}

	@RequestMapping(value = "/Book", method = RequestMethod.POST)
	public ModelAndView addBook(@Valid @ModelAttribute("command") Book book, BindingResult bindingResult,
			ModelMap model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("method", "post");
			model.addAttribute("authorList", authorService.list()); 
			return new ModelAndView("form_book", bindingResult.getModel());
		}
		int id = bookService.save(book.getTitle(), book.getAuthor().getId(), book.getPublicationDate());
		model.addAttribute("id", id);
		return new ModelAndView("redirect:/Book/{id}");
	}

	@RequestMapping(value = "/Book", method = RequestMethod.DELETE)
	public ModelAndView deleteBook(@RequestParam int id, @RequestParam String title, ModelMap model) {
		bookService.deleteById(id);
		List<Book> listBooks = bookService.seek(title);
		model.addAttribute("list", listBooks);
		return new ModelAndView("result_seek_books", "command", new Book());
	}

	@RequestMapping(value = "/Book", method = RequestMethod.PUT)
	public ModelAndView updateBook(@Valid @ModelAttribute("command") Book book, BindingResult bindingResult,
			ModelMap model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("id", book.getId());
			model.addAttribute("authorList", authorService.list());
			model.addAttribute("method", "put");
			return new ModelAndView("form_book", bindingResult.getModel());
		}
		bookService.update(book.getId(), book.getTitle(), book.getAuthor().getId(), book.getPublicationDate());
		return new ModelAndView("redirect:/Book/" + book.getId());
	}

	@InitBinder	
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		sdf.setLenient(true);
		binder.registerCustomEditor(Date.class, "publicationDate", new CustomDateEditor(sdf, true));
	}

}
