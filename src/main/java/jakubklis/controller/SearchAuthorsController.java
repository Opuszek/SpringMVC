package jakubklis.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import jakubklis.model.Author;
import jakubklis.service.AuthorService;

@Controller
public class SearchAuthorsController {

	@Autowired
	AuthorService authorService;

	@RequestMapping(value = "/searchAuthors", method = RequestMethod.GET)
	public ModelAndView searchAuthorsShowForm() {
		return new ModelAndView("seek_authors_form", "command", new Author());
	}

	@RequestMapping(value = "/searchAuthors", method = RequestMethod.POST)
	public ModelAndView createAuthorList(@RequestParam String keyword, ModelMap model) {
		List<Author> authorsList = authorService.seek(keyword);
		model.addAttribute("list", authorsList);
		return new ModelAndView("result_seek_authors", "command", new Author());
	}
}
