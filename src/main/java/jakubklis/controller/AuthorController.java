package jakubklis.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import jakubklis.model.Author;
import jakubklis.service.AuthorService;

@Controller
public class AuthorController {

	@Autowired
	AuthorService authorService;

	@RequestMapping(value = "/Author", method = RequestMethod.GET)
	public ModelAndView getAuthorSaveForm( ModelMap model) {
		model.addAttribute("method", "post");
		return new ModelAndView("form_author", "command", new Author());
	}
	
	@RequestMapping(value = "/Author", method = RequestMethod.GET, params="id")
    public ModelAndView getAuthorUpdateForm(@RequestParam Integer id,
            ModelMap model) {
        model.addAttribute("id", id);
        model.addAttribute("method", "put");
        return new ModelAndView("form_author", "command", new Author());
    }

	@RequestMapping(value = "/Author/{id}", method = RequestMethod.GET)
	public String getAuthorData(@PathVariable(value = "id") int id, ModelMap model) {
		Author author = authorService.getById(id);
		model.addAttribute("author", author);
		return "result_author";
	}

	@RequestMapping(value = "/Author", method = RequestMethod.POST)
	public ModelAndView addAuthor(@Valid @ModelAttribute("command") Author author, BindingResult bindingResult,
			ModelMap model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("method", "post");
			return new ModelAndView("form_author", bindingResult.getModel());
		}
		int id = authorService.save(author.getName(), author.getSurname());
		model.addAttribute("id", id);
		return new ModelAndView("redirect:/Author/{id}");
	}

	@RequestMapping(value = "/Author", method = RequestMethod.DELETE)
	public ModelAndView deleteAuthor(@RequestParam int id, @RequestParam String name, ModelMap model) {
		authorService.deleteById(id);
		List<Author> listAuthors = authorService.seek(name);
		model.addAttribute("list", listAuthors);
		return new ModelAndView("result_seek_authors", "command", new Author());
	}

	@RequestMapping(value = "/Author", method = RequestMethod.PUT)
	public String updateAuthor(@Valid @ModelAttribute("command") Author author, BindingResult bindingResult,
			ModelMap model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("method", "put");
			model.addAttribute("id", author.getId());
			return "form_author";
		}
		authorService.update(author.getId(), author.getName(), author.getSurname());
		return "redirect:/Author/" + author.getId();
	}
}
