package jakubklis.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import jakubklis.model.Book;
import jakubklis.service.BookService;

@Controller
public class SearchBookController {
	
	@Autowired
	BookService bookService;
	
	@RequestMapping(value = "/searchBooks", method = RequestMethod.GET)
	public ModelAndView searchBooksShowForm() {
		return new ModelAndView("seek_books_form", "command", new Book());
	}
	
	@RequestMapping(value = "/searchBooks", method = RequestMethod.POST)
	public ModelAndView createBookList(@RequestParam String keyword, ModelMap model) {
		List<Book> listBooks = bookService.seek(keyword);
		model.addAttribute("list", listBooks);
		return new ModelAndView("result_seek_books", "command", new Book());
	}
}
