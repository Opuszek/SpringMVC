package jakubklis.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@ControllerAdvice
public class GlobalControllerExceptionHandler {

      
    @ExceptionHandler(MySQLIntegrityConstraintViolationException.class)
    public ModelAndView handleConstraintViolation() {
        ModelAndView mv = new ModelAndView();
        mv.addObject("errorMsg", "Data integrity exception has occured");
        mv.setViewName("error_page");
        return mv;
    }
    
}
