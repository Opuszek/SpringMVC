package jakubklis.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jakubklis.model.Author;

@Repository("AuthorDao")
@Transactional
public class AuthorDaoImpl extends AbstractDao implements AuthorDao {

    @Override
    public int save(Author Author) {
        return saveAbstract(Author);
    }

    @Override
    public void update(Author Author) {
        getSession().update(Author);
    }

    @Override
    public boolean deleteById(int id) {
        return deletePersistentObjectById(Author.class, id);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Author> list() {
        Criteria criteria = getSession().createCriteria(Author.class);
        return (List<Author>) sortAuthorListAccordingToNameAndSurname(criteria.list());
    }

    @Override
    public Author get(int id) {
        return (Author) getSession().get(Author.class, id);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Author> seek(String string) {
        Criteria criteria = getSession().createCriteria(Author.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.add(Restrictions.disjunction().add(Restrictions.like("name", "%" + string + "%"))
                .add(Restrictions.like("surname", "%" + string + "%")));
        return  sortAuthorListAccordingToNameAndSurname(criteria.list());
    }
    
    @Override
    public Author find(String name, String surname) {
        Criteria criteria = getSession().createCriteria(Author.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .add(Restrictions.like("name", name))
                .add(Restrictions.like("surname", surname));
        return (Author) criteria.uniqueResult();
    }

    private List<Author> sortAuthorListAccordingToNameAndSurname(List<Author> authorList) {
        authorList.sort(
                (Author firstAuthor, Author secondAuthor) -> firstAuthor.getSurname().compareTo(secondAuthor.getSurname()));
        authorList.sort(
                (Author firstAuthor, Author secondAuthor) -> firstAuthor.getName().compareTo(secondAuthor.getName()));
        return authorList;
    }

}