package jakubklis.dao;

import java.util.List;

import jakubklis.model.Author;

public interface AuthorDao {
	
	int save (Author author);
	Author get (int Id);
	boolean deleteById (int Id);
	List<Author> list();
	void update (Author author);
	List<Author> seek (String string);
	Author find(String name, String surname);
} 
