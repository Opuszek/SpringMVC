package jakubklis.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jakubklis.model.User;

@Repository("userDao")
@Transactional
public class UserDaoImpl extends AbstractDao implements UserDao {

	public int save(User user) {
		return saveAbstract(user);
	}
	
	public User getUserByNick (String nick){
		Criteria criteria = getSession().createCriteria(User.class);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.like("nick", nick));
		return  (User) criteria.uniqueResult();
	}

}
