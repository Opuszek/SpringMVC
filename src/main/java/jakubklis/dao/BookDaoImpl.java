package jakubklis.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jakubklis.model.Author;
import jakubklis.model.Book;

@Repository("bookDao")
@Transactional
public class BookDaoImpl extends AbstractDao implements BookDao {

    @Override
    public int save(Book book) {
        return saveAbstract(book);
    }

    @Override
    public void update(Book book) {
        getSession().update(book);
    }

    @Override
    public void deleteById(int id) {
        Book book = get(id);
        if (book == null)
            throw new org.hibernate.ObjectNotFoundException(id, "jakubklis.model.Book");
        Author author = book.getAuthor();
        if (author!=null) author.getBookSet().remove(book);
        delete(book);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Book> list() {
        Criteria criteria = getSession().createCriteria(Book.class);
        return (List<Book>) sortBookSetAccordingToTitle(criteria.list());
    }

    @Override
    public Book get(int id) {
        return (Book) getSession().get(Book.class, id);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Book> seek(String string) {
        Criteria criteria = getSession().createCriteria(Book.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.createAlias("author", "authorAlias");
        criteria.add(Restrictions.disjunction().add(Restrictions.like("title", "%" + string + "%"))
                .add(Restrictions.like("authorAlias.name", "%" + string + "%"))
                .add(Restrictions.like("authorAlias.surname", "%" + string + "%")));
        return sortBookSetAccordingToTitle(criteria.list());
    }
    
    @Override
   public Book find(String title, Date date) {
       Criteria criteria = getSession().createCriteria(Book.class);
       criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
       criteria.createAlias("author", "authorAlias");
       criteria.add(Restrictions.like("title", "%" + title + "%")).add(Restrictions.eq("publicationDate", date));
       return (Book) criteria.uniqueResult();
   }

    private List<Book> sortBookSetAccordingToTitle(List<Book> bookSet) {
        bookSet.sort((Book firstBook, Book secondBook) -> firstBook.getTitle().compareTo(secondBook.getTitle()));
        return bookSet;
    }
}