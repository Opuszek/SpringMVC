package jakubklis.dao;

import java.io.Serializable;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractDao {

	@Autowired
	private SessionFactory sessionFactory;

	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public int saveAbstract(Object entity) {
		 return (int) getSession().save(entity);
	}

	protected void delete(Object entity) {
		getSession().delete(entity);
	}

	public boolean deletePersistentObjectById(Class<?> type, Serializable id) {
		try {
			Object persistentInstance = getSession().load(type, id);
			delete(persistentInstance);
			return true;
		} catch (org.hibernate.ObjectNotFoundException e) {
			return false;
		}
	}
}