package jakubklis.dao;

import jakubklis.model.User;

public interface UserDao {
	
	public User getUserByNick(String name);
	public int save(User user);
	
}
