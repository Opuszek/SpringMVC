package jakubklis.dao;

import java.util.Date;
import java.util.List;

import jakubklis.model.Book;

public interface BookDao {
	
	int save (Book book);
	Book get (int id);
	void deleteById (int id);
	List<Book> list();
	void update (Book book);
	List<Book> seek(String string);
	Book find (String title, Date publicationDate);
}
