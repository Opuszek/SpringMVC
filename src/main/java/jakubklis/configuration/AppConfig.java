package jakubklis.configuration;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@ImportResource("classpath:spring-security.xml")
@ComponentScan(basePackages = {
		"jakubklis" }, excludeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern = "jakubklis.main."))
public class AppConfig {
	
	@Bean
	@Scope("prototype")
	Logger logger(InjectionPoint injectionPoint) throws IOException {
		return LogManager.getLogger(injectionPoint.getMethodParameter().getContainingClass().getName());
	}
	
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		return bCryptPasswordEncoder;
}
}
