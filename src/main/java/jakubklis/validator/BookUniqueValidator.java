package jakubklis.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import jakubklis.model.Book;
import jakubklis.service.BookService;

public class BookUniqueValidator implements
ConstraintValidator<UniqueEntityConstraint, Book> {

    
    @Autowired
    BookService bookService;
    
    @Override
    public void initialize (UniqueEntityConstraint UniqueEntityConstraint) {
        
    }
    
    @Override
    public boolean isValid (Book book, ConstraintValidatorContext cxt) {
        return !bookService.exists(book);
    }
    
    
    
    
    
    
}
