package jakubklis.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import jakubklis.model.Author;
import jakubklis.service.AuthorService;

public class AuthorUniqueValidator implements
ConstraintValidator<UniqueEntityConstraint, Author> {

    
    @Autowired
    AuthorService authorService;
    
    @Override
    public void initialize (UniqueEntityConstraint UniqueEntityConstraint) {
        
    }
    
    @Override
    public boolean isValid (Author author, ConstraintValidatorContext cxt) {
        return !authorService.exists(author);
    }
    
    
    
    
    
    
}
