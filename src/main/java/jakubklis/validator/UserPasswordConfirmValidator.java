package jakubklis.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import jakubklis.model.User;

public class UserPasswordConfirmValidator implements ConstraintValidator<PasswordConfirmConstraint, User> {
    
    @Override
    public void initialize(PasswordConfirmConstraint annotation) {
    }
    
    @Override
    public boolean isValid(User user, ConstraintValidatorContext ctx) {
        String password = user.getPassword();
        String confirmPassword = user.getConfirmPassword();
        if (password.equals(confirmPassword)) return true;
        else return false;
    }
    

}
