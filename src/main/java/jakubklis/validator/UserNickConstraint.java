package jakubklis.validator;

import java.lang.annotation.Documented;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = UserNickValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface UserNickConstraint {
    String message() default "Chosen nick is already in use";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
