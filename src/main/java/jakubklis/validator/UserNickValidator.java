package jakubklis.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import jakubklis.service.UserService;

public class UserNickValidator implements
ConstraintValidator<UserNickConstraint, String> {
    
    @Autowired
    UserService userService;

  @Override
  public void initialize(UserNickConstraint userNickContraint) {
  }

  @Override
  public boolean isValid(String nick,
          ConstraintValidatorContext cxt) {
      return (userService.getUserByNick(nick)==null) ? true : false;
  }

}