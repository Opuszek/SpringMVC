package jakubklis.validator;

import java.lang.annotation.Documented;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Documented
@Constraint(validatedBy = UserPasswordConfirmValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface PasswordConfirmConstraint {
    
    String message() default "Password is not confirmed properly";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
