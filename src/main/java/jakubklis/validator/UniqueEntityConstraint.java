package jakubklis.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target(ElementType.TYPE)
@Documented
@Constraint(validatedBy = {AuthorUniqueValidator.class, BookUniqueValidator.class})
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueEntityConstraint {
    
    String message() default "Submitted entity duplicates already existing one";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}

