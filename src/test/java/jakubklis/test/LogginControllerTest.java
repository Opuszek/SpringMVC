package jakubklis.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import jakubklis.configuration.AppConfig;
import jakubklis.dao.AuthorDao;
import jakubklis.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextHierarchy({
        @ContextConfiguration(loader = AnnotationConfigWebContextLoader.class, classes = { AppConfig.class }),
        @ContextConfiguration({ "file:src/main/webapp/WEB-INF/SpringMVC-servlet.xml" }) })
@Transactional
public class LogginControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private static MockMvc mockMvc;

    @Autowired
    UserService userService;

    @Autowired
    AuthorDao authorDao;

    @Before
    @WithMockUser(username = "username", roles = { "USER" })
    public void setEntities() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testRegisterUser() throws Exception {
        mockMvc.perform(
                post("/registration").contentType(MediaType.APPLICATION_FORM_URLENCODED).param("nick", "typicalnick")
                        .param("password", "typicalpassword").param("confirmPassword", "typicalpassword"))
                .andExpect(status().isFound()).andExpect(view().name("redirect:/"));
    }

    @Test
    public void testRegisterUserWithWrongConfirmedPassword() throws Exception {
        mockMvc.perform(
                post("/registration").contentType(MediaType.APPLICATION_FORM_URLENCODED).param("nick", "typicalnick")
                        .param("password", "typicalpassword").param("confirmPassword", "wrongpassword"))
                .andExpect(model().attributeHasFieldErrors("command")).andExpect(status().isOk())
                .andExpect(view().name("/registration"));
    }


}
