package jakubklis.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import jakubklis.configuration.AppConfig;
import jakubklis.dao.AuthorDao;
import jakubklis.model.Author;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfig.class })
@Transactional
public class AuthorDaoTest {

    @Autowired
    AuthorDao authorDao;

    final String NAME = "Test Name";
    final String SURNAME = "Test Surname";
    final String UPDATED_NAME = "Updated Name";
    final String SEARCHED_VALUE = "Searched Value";



    @Test
    public void entitiesShouldBeSavedIntoDatabase() {
        int id = authorDao.save(createAuthor(NAME,SURNAME));
        Author author = authorDao.get(id);
        assertEquals(NAME, author.getName());
        assertEquals(SURNAME, author.getSurname());
    }

    @Test
    public void authorShouldBeUpdatedInDatabase() {
        int id = authorDao.save(createDefaultAuthor());

        Author updatedAuthor = authorDao.get(id);
        updatedAuthor.setName(UPDATED_NAME);
        authorDao.update(updatedAuthor);

        Author actualAuthor = authorDao.get(id);

        assertEquals(UPDATED_NAME, actualAuthor.getName());
    }

    @Test
    public void authorShouldBeDeletedInDatabase() {
        int id = authorDao.save(createDefaultAuthor());
        authorDao.deleteById(id);
        assertEquals(null, authorDao.get(id));
    }
    
    private Author createDefaultAuthor() {
        Author author = new Author();
        author.setName("Default Name");
        author.setSurname("Default Surname");

        return author;

    }
    
    private Author createAuthor(String name, String surname) {
        Author author = new Author();
        author.setName(name);
        author.setSurname(surname);

        return author;

    }

}
