package jakubklis.test;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import jakubklis.configuration.AppConfig;
import jakubklis.dao.AuthorDao;
import jakubklis.dao.BookDao;
import jakubklis.model.Book;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfig.class })
@Transactional
public class BookDaoTest {
    
    final String TITLE = "title";
    final Date PUBLICATION_DATE = new Date(1);
    final String UPDATED_TITLE = "updated_title";
    
    @Autowired
    BookDao bookDao;
    
    @Autowired
    AuthorDao authorDao;
    
    @Test
    public void entitiesShouldBeSavedToDatabase () {
        int id = bookDao.save(createBook(TITLE,PUBLICATION_DATE));
        Book book = bookDao.get(id);
        assertEquals(TITLE, book.getTitle());
        assertEquals(PUBLICATION_DATE, book.getPublicationDate());
    }
    
    @Test
    public void entitiesShouldBeUpdatedInDatabase () {
        int id = bookDao.save(createBook(TITLE,PUBLICATION_DATE));
        
        Book updatedBook = bookDao.get(id);
        updatedBook.setTitle(UPDATED_TITLE);
        bookDao.update(updatedBook);
        
        Book actualBook = bookDao.get(id);
        assertEquals(UPDATED_TITLE, actualBook.getTitle());
        }
    
    @Test
    public void entitiesShouldBeDeletedInDatabase () {
        int id = bookDao.save(createBook(TITLE,PUBLICATION_DATE));
        
        bookDao.deleteById(id);
        Book book = bookDao.get(id);
        assertEquals(null, book);
    }
    
    
    
    private Book createBook(String title, Date publicationDate) {
        Book book = new Book();
        book.setTitle(title);
        book.setPublicationDate(publicationDate);
        return book;
    }
}
