package jakubklis.test;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import jakubklis.configuration.AppConfig;
import jakubklis.dao.AuthorDao;
import jakubklis.model.Author;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextHierarchy({
        @ContextConfiguration(loader = AnnotationConfigWebContextLoader.class, classes = { AppConfig.class }),
        @ContextConfiguration({ "file:src/main/webapp/WEB-INF/SpringMVC-servlet.xml" }) })
@Transactional
public class AuthorControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private static MockMvc mockMvc;

    @Autowired
    AuthorDao authorDao;

    final String NAME = "Test Name";
    final String SURNAME = "Test Surname";
    final String DUPLICATEDNAME = "DUPLICATEDNAME";
    final String DUPLICATEDSURNAME = "DUPLICATEDSURNAME";
    final int CHARACTERS_LIMIT_NAME = 40;
    final int CHARACTERS_LIMIT_SURNAME = 40;

    @Before
    @WithMockUser(username = "username", roles = { "USER" })
    public void setEntities() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        fillDatabase();
    }

    @Test
    @WithMockUser(username = "username", roles = { "USER" })
    public void testGetSaveAuthorPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/Author")).andExpect(model().attribute("method", "post"))
                .andExpect(status().isOk()).andExpect(forwardedUrl("/jsp/form_author.jsp"));
    }


    @Test
    @WithMockUser(username = "username", roles = { "USER" })
    public void testGetUpdateAuthorPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/Author").param("id", "1"))
                .andExpect(model().attribute("method", "put")).andExpect(status().isOk())
                .andExpect(forwardedUrl("/jsp/form_author.jsp"));
    }

    @Test
    @WithMockUser(username = "username", roles = { "USER" })
    public void testPostAuthor() throws Exception {
        mockMvc.perform(post("/Author").contentType(MediaType.APPLICATION_FORM_URLENCODED).param("name", NAME)
                .param("surname", SURNAME)).andExpect(status().isFound())
                .andExpect(view().name("redirect:/Author/{id}"));
    }

    @Test
    @WithMockUser(username = "username", roles = { "USER" })
    public void testPutAuthor() throws Exception {
        String id = getAuthorIdAsString();
        mockMvc.perform(put("/Author").contentType(MediaType.APPLICATION_FORM_URLENCODED).param("id", id)
                .param("name", "name_testPutAuthor").param("surname", "Surname_testPutAuthor"))
                .andExpect(status().isFound()).andExpect(view().name("redirect:/Author/" + id));
    }

    @Test
    @WithMockUser(username = "username", roles = "USER")
    public void testAuthorGetPage() throws Exception {
        int id = getAuthorId();
        Author author = authorDao.get(id);
        mockMvc.perform(get("/Author/" + id))
                .andExpect(model().attribute("author",
                        allOf(hasProperty("name", equalTo(author.getName())),
                                hasProperty("surname", equalTo(author.getSurname())))))
                .andExpect(status().isOk()).andExpect(forwardedUrl("/jsp/result_author.jsp"));
    }

    @Test
    @WithMockUser(username = "username", roles = { "USER" })
    public void testPostTooLongAuthorName() throws Exception {
        mockMvc.perform(post("/Author").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("name", createTooLongNameString()).param("surname", SURNAME))
                .andExpect(model().attributeHasFieldErrors("command", "name")).andExpect(status().isOk())
                .andExpect(view().name("form_author"));
    }

    @Test
    @WithMockUser(username = "username", roles = { "USER" })
    public void testPostExistingAuthor() throws Exception {
        mockMvc.perform(post("/Author").contentType(MediaType.APPLICATION_FORM_URLENCODED).param("name", DUPLICATEDNAME)
                .param("surname", DUPLICATEDSURNAME)).andExpect(model().attributeHasFieldErrors("command"))
                .andExpect(status().isOk()).andExpect(view().name("form_author"));
    }

    @Test
    @WithMockUser(username = "username", roles = { "USER" })
    public void testPostTooLongAuthorSurname() throws Exception {
        mockMvc.perform(post("/Author").contentType(MediaType.APPLICATION_FORM_URLENCODED).param("name", NAME)
                .param("surname", createTooLongSurnameString()))
                .andExpect(model().attributeHasFieldErrors("command", "surname")).andExpect(status().isOk())
                .andExpect(view().name("form_author"));
    }

    @Test
    @WithMockUser(username = "username", roles = { "USER" })
    public void testPostEmptyAuthorName() throws Exception {
        mockMvc.perform(post("/Author").contentType(MediaType.APPLICATION_FORM_URLENCODED).param("name", "")
                .param("surname", SURNAME)).andExpect(model().attributeHasFieldErrors("command", "name"))
                .andExpect(status().isOk()).andExpect(view().name("form_author"));
    }

    @Test
    @WithMockUser(username = "username", roles = { "USER" })
    public void testPostEmptyAuthorSurname() throws Exception {
        mockMvc.perform(post("/Author").contentType(MediaType.APPLICATION_FORM_URLENCODED).param("name", NAME)
                .param("surname", "")).andExpect(model().attributeHasFieldErrors("command", "surname"))
                .andExpect(status().isOk()).andExpect(view().name("form_author"));
    }


    private void fillDatabase() {
        Author author = createTestAuthor();
        authorDao.save(author);
    }

    private Author createTestAuthor() {
        Author author = new Author();
        author.setName(DUPLICATEDNAME);
        author.setSurname(DUPLICATEDSURNAME);
        return author;
    }


    private int getAuthorId() {
        List<Author> list = authorDao.list();
        return list.get(0).getId();
    }

    private String getAuthorIdAsString() {
        return Integer.toString(getAuthorId());
    }

    private String createTooLongNameString() {
        String name = null;
        for (int i = 0; i < CHARACTERS_LIMIT_NAME + 1; i++) {
            name += "a";
        }
        return name;
    }

    private String createTooLongSurnameString() {
        String name = null;
        for (int i = 0; i < CHARACTERS_LIMIT_SURNAME + 1; i++) {
            name += "a";
        }
        return name;
    }

}
