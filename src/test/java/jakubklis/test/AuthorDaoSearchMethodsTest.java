package jakubklis.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import jakubklis.configuration.AppConfig;
import jakubklis.dao.AuthorDao;
import jakubklis.model.Author;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfig.class })
@Transactional
public class AuthorDaoSearchMethodsTest {

    String INSTANCENAME = "NAME2";
    String INSTANCESURNAME = "SURNAME2";
    
    @Autowired
    AuthorDao authorDao;

    @Before
    public void setUpEntities() {
        fillDatabase();
    }

    @Test
    public void searchByNameShouldReturnAuthors() {
        List<Author> authors = authorDao.seek(INSTANCENAME);
        
        assert (authors.size() == 1);
        assertEquals(INSTANCENAME, authors.get(0).getName());
        assertEquals(INSTANCESURNAME, authors.get(0).getSurname());
    }
    
    @Test
    public void findAuthorShouldReturnSingularAuthor() {
        Author author = authorDao.find(INSTANCENAME, INSTANCESURNAME);
        
        assertEquals(INSTANCENAME, author.getName());
        assertEquals(INSTANCESURNAME, author.getSurname());
    }
    
    @Test
    public void findAuthorShouldNotReturnAuthorIfSurnameIsInvalid() {
        Author author = authorDao.find(INSTANCENAME, "InvalidValue");
        
        assertNull(author);
    }

    @Test
    public void searchBySurnameShouldReturnAuthors() {
        List<Author> authors = authorDao.seek(INSTANCESURNAME);
        assert (authors.size() == 1);
        assertEquals(INSTANCENAME, authors.get(0).getName());
        assertEquals(INSTANCESURNAME, authors.get(0).getSurname());
    }

    @Test
    public void methodListReturnsAllEntities() {
        List<Author> authors = authorDao.list();
        assert (authors.size() == 3);
        assert (authors.containsAll(defaultDatabaseState()));
    }

    @Test
    public void authorListShouldBeSortedAccordingToNameAndSurname() {
        List<Author> expectedList = authorDao.list();
        sortAuthorListAccordingToNameAndSurname(expectedList);
        
        List<Author> list = authorDao.list();
        for (int i = 0; i<list.size(); i++) {
        assertEquals (expectedList.get(i), list.get(i));   
        }
    }

    private void fillDatabase() {
        authorDao.save(createAuthor("NAME2", "SURNAME2"));
        authorDao.save(createAuthor("NAME1", "SURNAME1"));
        authorDao.save(createAuthor("NAME3", "SURNAME3"));
    }

    private List<Author> defaultDatabaseState() {
        List<Author> list = new ArrayList<Author>();
        list.add(createAuthor("NAME1", "SURNAME1"));
        list.add(createAuthor("NAME3", "SURNAME3"));
        list.add(createAuthor("NAME2", "SURNAME2"));
        return list;
    }

    private Author createAuthor(String name, String surname) {
        Author author = new Author();
        author.setName(name);
        author.setSurname(surname);

        return author;

    }

    private List<Author> sortAuthorListAccordingToNameAndSurname(List<Author> authorList) {
        authorList.sort((Author firstAuthor, Author secondAuthor) -> firstAuthor.getSurname()
                .compareTo(secondAuthor.getSurname()));
        authorList.sort(
                (Author firstAuthor, Author secondAuthor) -> firstAuthor.getName().compareTo(secondAuthor.getName()));
        return authorList;
    }

}
