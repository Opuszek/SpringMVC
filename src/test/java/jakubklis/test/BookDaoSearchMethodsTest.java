package jakubklis.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import jakubklis.configuration.AppConfig;
import jakubklis.dao.AuthorDao;
import jakubklis.dao.BookDao;
import jakubklis.model.Author;
import jakubklis.model.Book;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfig.class })
@Transactional
public class BookDaoSearchMethodsTest {

    final String INSTANCETITLE = "TITLE2";
    final Date INSTANCEDATE = createDate("10/10/2003");
    final Date INVALIDDATE = createDate("10/10/2004");
    
    @Autowired
    BookDao bookDao;
    
    @Autowired
    AuthorDao authorDao;
    
    Logger logger = LogManager.getLogger(BookDaoSearchMethodsTest.class);
    
    
    @Before
    public void setUpEntities() {
        fillDatabase();
    }

    @Test
    public void searchByTitleShouldReturnBooks() {
        List<Book> books = bookDao.seek(INSTANCETITLE);
        assert (books.size() == 1);
        assertEquals(INSTANCETITLE, books.get(0).getTitle());
        assertEquals(INSTANCEDATE, books.get(0).getPublicationDate());
    }

    @Test
    public void methodListReturnsAllEntities() {
        List<Book> books = bookDao.list();
        assert (books.size() == 3);
        assert (books.containsAll(defaultDatabaseState()));
    }

    @Test
    public void bookListShouldBeSortedAccordingToNameAndSurname() {
        List<Book> expectedList = bookDao.list();
        sortBookListAccordingToTitle(expectedList);
        
        List<Book> list = bookDao.list();
        for (int i = 0; i<list.size(); i++) {
        assertEquals (expectedList.get(i), list.get(i));   
        }
    }
    
    @Test
    public void findBookShouldReturnBook() {
        Book book = bookDao.find(INSTANCETITLE, INSTANCEDATE);
        
        assertNotNull (book);
        assertEquals(INSTANCETITLE, book.getTitle());
        assertEquals(INSTANCEDATE, book.getPublicationDate());
    }
    
    @Test
    public void findBookWithInvalidDateShouldNotReturnBook() {
        Book book = bookDao.find(INSTANCETITLE, INVALIDDATE);
        
        assertNull (book);
    }
    
    @Test
    public void findBookWithInvalidTitleShouldNotReturnBook() {
        Book book = bookDao.find("INVALIDDATE", INSTANCEDATE);
        
        assertNull (book);
    }

    private void fillDatabase() {
        saveAuthor();
        bookDao.save(createBook("TITLE2", createDate("10/10/2003")));
        bookDao.save(createBook("TITLE1", createDate("10/10/2001")));
        bookDao.save(createBook("TITLE3", createDate("10/10/2002")));
    }

    private List<Book> defaultDatabaseState() {
        List<Book> list = new ArrayList<Book>();
        list.add(createBook("TITLE1", createDate("10/10/2001")));
        list.add(createBook("TITLE3", createDate("10/10/2002")));
        list.add(createBook("TITLE2", createDate("10/10/2003")));
        return list;
    }

    private Book createBook(String title, Date publicationDate) {
        Book book = new Book();
        book.setTitle(title);
        book.setPublicationDate(publicationDate);
        book.setAuthor(getAuthor());

        return book;

    }
    
    private Author getAuthor () {
        List<Author>list = authorDao.list();
        return list.get(0);
    }

    private List<Book> sortBookListAccordingToTitle(List<Book> bookList) {
        bookList.sort((Book firstBook, Book secondBook) -> firstBook.getTitle().compareTo(secondBook.getTitle()));
        return bookList;
    }
    
    private void saveAuthor() {
        Author author = new Author();
        author.setName("name");
        author.setSurname("surname");
        authorDao.save(author);
    }
    
    public Date createDate (String string)  {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        try {date = sdf.parse(string);}
        catch (ParseException e) {
            logger.log( Level.ERROR, "SimpleDateFormat couldn't parse value " + string );
        }
        return date;
    }
}