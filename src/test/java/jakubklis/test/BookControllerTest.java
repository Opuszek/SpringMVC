package jakubklis.test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import jakubklis.configuration.AppConfig;
import jakubklis.dao.AuthorDao;
import jakubklis.dao.BookDao;
import jakubklis.model.Author;
import jakubklis.model.Book;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextHierarchy({
        @ContextConfiguration(loader = AnnotationConfigWebContextLoader.class, classes = { AppConfig.class }),
        @ContextConfiguration({ "file:src/main/webapp/WEB-INF/SpringMVC-servlet.xml" }) })
@Transactional
public class BookControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    AuthorDao authorDao;

    @Autowired
    BookDao bookDao;

    private static MockMvc mockMvc;

    final String DUPLICATEDTITLE = "Test Title";
    final String DUPLICATEDDATE = "11-10-2010";
    final String DATE = "23-11-2011";
    final int CHARACTER_LIMIT_TITLE = 40;

    Logger logger = LogManager.getLogger(AuthorControllerTest.class);

    @Before
    @WithMockUser(username = "username", roles = { "USER" })
    public void setEntities() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        fillDatabase();
    }

    @Test
    @WithMockUser(username = "username", roles = { "USER" })
    public void testGetSaveBookPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/Book")).andExpect(model().attribute("method", "post"))
                .andExpect(status().isOk()).andExpect(forwardedUrl("/jsp/form_book.jsp"));
    }

    @Test
    @WithMockUser(username = "username", roles = { "USER" })
    public void testGetUpdateBookPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/Book").param("id", "1"))
                .andExpect(model().attribute("method", "put")).andExpect(status().isOk())
                .andExpect(forwardedUrl("/jsp/form_book.jsp"));
    }

    @Test
    @WithMockUser(username = "username", roles = { "USER" })
    public void testPostBook() throws Exception {
        mockMvc.perform(post("/Book").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("author.id", getAuthorIdAsString()).param("title", "Title_testPostBook")
                .param("publicationDate", DATE)).andExpect(status().isFound())
                .andExpect(view().name("redirect:/Book/{id}"));
    }

    @Test
    @WithMockUser(username = "username", roles = { "USER" })
    public void testPutBook() throws Exception {
        String id = getBookIdAsString();
        mockMvc.perform(put("/Book").contentType(MediaType.APPLICATION_FORM_URLENCODED).param("id", id)
                .param("author.id", getAuthorIdAsString()).param("title", "Title_testPutBook")
                .param("publicationDate", DATE)).andExpect(status().isFound())
                .andExpect(view().name("redirect:/Book/" + id));
    }

    @Test
    @WithMockUser(username = "username", roles = "USER")
    public void testBookGetPage() throws Exception {
        int id = getBookId();
        Book book = bookDao.get(id);
        mockMvc.perform(get("/Book/" + id))
                .andExpect(model().attribute("book", hasProperty("title", equalTo(book.getTitle()))))
                .andExpect(status().isOk()).andExpect(forwardedUrl("/jsp/result_book.jsp"));
    }

    @Test
    @WithMockUser(username = "username", roles = { "USER" })
    public void testPostExistingBook() throws Exception {
        mockMvc.perform(post("/Book").contentType(MediaType.APPLICATION_FORM_URLENCODED).param("title", DUPLICATEDTITLE)
                .param("publicationDate", DUPLICATEDDATE).param("author.id", getAuthorIdAsString()))
                .andExpect(model().attributeHasFieldErrors("command")).andExpect(status().isOk())
                .andExpect(view().name("form_book"));
    }

    @Test
    @WithMockUser(username = "username", roles = { "USER" })
    public void testPostBookWithTooLongTitle() throws Exception {
        mockMvc.perform(
                post("/Book").contentType(MediaType.APPLICATION_FORM_URLENCODED).param("title", createTooLongTitle())
                        .param("publicationDate", DATE).param("author.id", getAuthorIdAsString()))
                .andExpect(model().attributeHasFieldErrors("command", "title")).andExpect(status().isOk())
                .andExpect(view().name("form_book"));

    }
    
    @Test
    @WithMockUser(username = "username", roles = { "USER" })
    public void testPostBookWithFuturePublicationDate() throws Exception {
        mockMvc.perform(
                post("/Book").contentType(MediaType.APPLICATION_FORM_URLENCODED).param("title", "title")
                        .param("publicationDate", getFutureDateAsString()).param("author.id", getAuthorIdAsString()))
                .andExpect(model().attributeHasFieldErrors("command", "publicationDate")).andExpect(status().isOk())
                .andExpect(view().name("form_book"));

    }
    
    @Test
    @WithMockUser(username = "username", roles = { "USER" })
    public void testPostBookWithInvalidDateInput() throws Exception {
        mockMvc.perform(
                post("/Book").contentType(MediaType.APPLICATION_FORM_URLENCODED).param("title", "title")
                        .param("publicationDate", "InvalidInput").param("author.id", getAuthorIdAsString()))
                .andExpect(model().attributeHasFieldErrors("command", "publicationDate")).andExpect(status().isOk())
                .andExpect(view().name("form_book"));

    }

    private Book createTestBook() {
        Book book = new Book();
        book.setTitle(DUPLICATEDTITLE);
        book.setPublicationDate(getDate());
        return book;
    }

    private Author createTestAuthor() {
        Author author = new Author();
        author.setName("testName");
        author.setSurname("testSurname");
        return author;
    }

    private void fillDatabase() {
        Book book = createTestBook();
        Author author = createTestAuthor();
        author.setBookSet(new HashSet<Book>(Arrays.asList(book)));
        book.setAuthor(author);
        authorDao.save(author);
    }

    private Date getDate() {
        Date date = new Date();
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        format.setLenient(true);
        try {
            date = format.parse(DUPLICATEDDATE);
        } catch (ParseException e) {
            logger.log(Level.ERROR, "SimpleDateFormat couldn't parse value " + DUPLICATEDDATE);
        }
        return date;
    }

    private int getBookId() {
        List<Book> list = bookDao.list();
        return list.get(0).getId();
    }

    private String getBookIdAsString() {
        return Integer.toString(getBookId());
    }

    private int getAuthorId() {
        List<Author> list = authorDao.list();
        return list.get(0).getId();
    }

    private String getAuthorIdAsString() {
        return Integer.toString(getAuthorId());
    }

    private String createTooLongTitle() {
        String title = "";
        for (int i = 0; i < CHARACTER_LIMIT_TITLE + 1; i++) {
            title += "a";
        }
        return title;
    }
    
    private String getFutureDateAsString() {
        Date date = new Date ();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, 1);
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        return format.format(cal.getTime());
        
    }

}
