package jakubklis.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.TreeMap;

import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.ContextConfiguration;

import jakubklis.configuration.AppConfig;
import jakubklis.dao.AuthorDaoImpl;
import jakubklis.dao.BookDaoImpl;
import jakubklis.model.Author;
import jakubklis.model.Book;

@ContextConfiguration(classes = { AppConfig.class })
public class StateDaoTest {

	@InjectMocks
	AuthorDaoImpl authorDao;

	@InjectMocks
	BookDaoImpl bookDao;

	@Mock
	Session mockSession;

	@Mock
	SessionFactory mockSessionFactory;

	TreeMap<Integer, Author> databaseAuthor = new TreeMap<Integer, Author>();

	TreeMap<Integer, Book> databaseBook = new TreeMap<Integer, Book>();

	private final String NAME = "Test Name";
	private final String SURNAME = "Test Surname";
	private final String TITLE = "Test Title";
	private final String UPDATED_NAME = "Updated Name";
	private final String UPDATED_TITLE = "UpdatedTitle";

	@Before
	public void setEntities() {
		initMocks(this);
		when(mockSessionFactory.getCurrentSession()).thenReturn(mockSession);
		when(mockSession.get(eq(Author.class), anyInt())).thenAnswer(new Answer<Author>() {
			@Override
			public Author answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				return databaseAuthor.get((Integer) args[1]);
			}
		});
		when(mockSession.load(eq(Author.class), anyInt())).thenAnswer(new Answer<Author>() {
			@Override
			public Author answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				int id = (Integer) args[1];
				Author author = databaseAuthor.get(id);
				if (author == null)
					throw new ObjectNotFoundException(id, args[0].getClass().getName());
				return author;
			}
		});

		when(mockSession.save(isA(Author.class))).thenAnswer(new Answer<Integer>() {
            @Override
            public Integer answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                Author author = (Author) args[0];
                int id = getNextAuthorId();
                author.setId(id);
                databaseAuthor.put(id, author);
                return id;
            }
        });

		doAnswer(new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				Integer id = ((Author) args[0]).getId();
				Author removedAuthor = databaseAuthor.remove(id);
				if (removedAuthor == null)
					throw new ObjectNotFoundException(id, args[0].getClass().getName());
				return null;
			}
		}).when(mockSession).delete(isA(Author.class));

		doAnswer(new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				Author author = (Author) args[0];
				Integer id = author.getId();
				databaseAuthor.put(id, author);
				return null;
			}
		}).when(mockSession).update(isA(Author.class));

		when(mockSession.get(eq(Book.class), anyInt())).thenAnswer(new Answer<Book>() {

			@Override
			public Book answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				return databaseBook.get((Integer) args[1]);
			}
		});

		when(mockSession.load(eq(Book.class), anyInt())).thenAnswer(new Answer<Book>() {
			@Override
			public Book answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				int id = (Integer) args[1];
				Book book = databaseBook.get(id);
				if (book == null)
					throw new ObjectNotFoundException(id, args[0].getClass().getName());
				return book;
			}
		});

		when(mockSession.save(isA(Book.class))).thenAnswer(new Answer<Integer>() {
            @Override
            public Integer answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                Book book = (Book) args[0];
                int id = getNextBookId();
                book.setId(id);
                databaseBook.put(id, book);
                return id;
            }
        });

		doAnswer(new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				Integer id = ((Book) args[0]).getId();
				Book removedBook = databaseBook.remove(id);
				if (removedBook == null)
					throw new ObjectNotFoundException(id, args[0].getClass().getName());
				return null;
			}
		}).when(mockSession).delete(isA(Book.class));

		doAnswer(new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				Book book = (Book) args[0];
				Integer id = book.getId();
				databaseBook.put(id, book);
				return null;
			}
		}).when(mockSession).update(isA(Book.class));
		Author author = new Author();
		author.setName(NAME);
		author.setSurname(SURNAME);
		List<Book> entitySet = new ArrayList<Book>();

		Book book = new Book();
		book.setTitle(TITLE);
		book.setAuthor(author);
		entitySet.add(book);
		author.setBookSet(new HashSet<Book>(entitySet));
		authorDao.save(author);
		bookDao.save(book);
	}

	@Test
	public void testSetEntities() {
		Author author = databaseAuthor.lastEntry().getValue();
		Book book = databaseBook.lastEntry().getValue();
		assert (databaseAuthor.size() == 1);
		assert (databaseBook.size() == 1);
		assertEquals(author.getName(), NAME);
		assertEquals(book.getTitle(), TITLE);
		assertEquals(book.getAuthor().getName(), NAME);
	}

	@Test
	public void testUpdateEntities() {

		int authorId = databaseAuthor.lastKey();
		Author updatedAuthor = new Author();
		updatedAuthor.setId(authorId);
		updatedAuthor.setName(UPDATED_NAME);

		int bookId = databaseBook.lastKey();
		Book updatedBook = new Book();
		updatedBook.setId(bookId);
		updatedBook.setTitle(UPDATED_TITLE);

		bookDao.update(updatedBook);
		authorDao.update(updatedAuthor);

		Author checkAuthor = databaseAuthor.lastEntry().getValue();
		Book checkBook = databaseBook.lastEntry().getValue();

		assertEquals(checkAuthor.getName(), UPDATED_NAME);
		assertEquals(checkBook.getTitle(), UPDATED_TITLE);
		assert (databaseAuthor.size() == 1);
		assert (databaseBook.size() == 1);
	}

	@Test
	public void testDelete() {
		int authorId = databaseAuthor.lastKey();
		int bookId = databaseBook.lastKey();

		authorDao.deleteById(authorId);
		bookDao.deleteById(bookId);

		assertTrue(databaseAuthor.isEmpty());
		assertTrue(databaseBook.isEmpty());

	}

	@After
	public void deleteEntities() {
		databaseAuthor.clear();
		databaseBook.clear();
	}

	private int getNextAuthorId() {
		return (!databaseAuthor.isEmpty()) ? databaseAuthor.lastKey() + 1 : 1;
	}

	private int getNextBookId() {
		return (!databaseBook.isEmpty()) ? databaseBook.lastKey() + 1 : 1;
	}

}
